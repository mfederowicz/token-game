<?php
//simple tokens board game model
class Game
{

    public $rows = 4;
    public $cols = 5;
    public $time = 60;
    public $tokens = array();

    public function __construct($rows, $cols)
    {
        if ($rows > 0) {
            $this->rows = $rows;
        }

        if ($cols > 0) {
            $this->cols = $cols;
        }


    }

    /**
     * @param int $cols
     */
    public function setCols($cols)
    {
        $this->cols = $cols;
    }

    /**
     * @return int
     */
    public function getCols()
    {
        return $this->cols;
    }

    /**
     * @param int $rows
     */
    public function setRows($rows)
    {
        $this->rows = $rows;
    }

    /**
     * @return int
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * Get selected Tokens on board
     * @return array
     */
    public function getSelectedTokens()
    {
        $selectedTokens = array();
        $sessionId = session_id();
        $gameData = $_SESSION[$sessionId];

        if (array_key_exists('tokens', $gameData)) {
            foreach ($gameData['tokens'] as $k => $token) {
                if (in_array($token['state'], array("show", "win"))) {
                    $selectedTokens[$token['id']] = $token;
                }
            }

        }


        return $selectedTokens;
    }
    /**
         * @return int;
         */
    public function getElapsedTime()
    {

        $sessionId = session_id();
        $gameData = $_SESSION[$sessionId];

        $finish = $gameData['finish'];

        if ($finish - time() < 0) {
            return 0;
        } else {
            return $finish - time();
        }
    }

    /**
     * @param string $tokenId
     * @return string;
     */
    public function checkToken($tokenId)
    {

        $sessionId = session_id();
        if (!array_key_exists($sessionId, $_SESSION)) {
            return "invalid game click start";
        }
        $gameData = $_SESSION[$sessionId];
        $finish = $gameData['finish'];
        if (time() > $finish) {
            session_destroy();
            return "game time limit exceeded";
        }

        if ($_SESSION[$sessionId]['player_wins']) {
            session_destroy();
            return "player wins";
        }
        if (intval($_SESSION[$sessionId]['tokens_showed']) >= 5) {
            session_destroy();
            return "try limit exceeded";
        }

        $tokens = $gameData['tokens'];
        if (!array_key_exists($tokenId, $tokens)) {
            session_destroy();
            return "invalid token";
        }
        $token = $tokens[$tokenId];
        if ($token['isWinning']) {
            $_SESSION[$sessionId]['player_wins'] = true;
            $_SESSION[$sessionId]['tokens'][$tokenId]['state'] = "win";
            session_commit();
            return "win";
        } else {
            $_SESSION[$sessionId]['tokens'][$tokenId]['state'] = "show";
            $_SESSION[$sessionId]['tokens_showed'] = intval($_SESSION[$sessionId]['tokens_showed']) + 1;
            session_commit();
            return "show";
        }

    }

    /**
     * Init game;
     */
    public function startGame()
    {

        session_start();
        session_regenerate_id(true);
        $sessionId = session_id();
        $this->tokens = array();


        for ($i = 0; $i < $this->getRows(); $i++) {
            for ($y = ($i % 2); $y < (($i % 2) + $this->getCols()); $y++) {
                $this->tokens["$i.$y"] = array('state' => 'hidden', 'isWinning' => false, 'id' => "$i.$y");
            }
        }

        $rand = array_rand($this->tokens);
        $this->tokens[$rand]['isWinning'] = true;


        $_SESSION[$sessionId]['tokens'] = $this->tokens;
        $_SESSION[$sessionId]['finish'] = time() + $this->time;
        session_commit();

        return "OK";
    }


}