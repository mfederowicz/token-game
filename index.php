<?php
session_start();
require_once 'Game.php';
//If we want play on larger board set size x,y params default 4x5 tokens :)
$game = new Game(4,5);

if(count($_GET) && isset($_GET['action'])){
    switch($_GET['action']){
        case 'checktoken':
            header('Content-type: application/json');
            $check = $game->checkToken($_GET['id']);
            echo json_encode($check);
            exit;
            break;
        case 'startgame':
            header('Content-type: application/json');
            $start = $game->startGame();
            echo json_encode($start);
            exit;
            break;
        default:
            exit;
            break;
    }
}
$selectedTokens = $game->getSelectedTokens();
$elapsedTime = $game->getElapsedTime();
?>
<html>
<head>
<title>Token Game</title>
<link href="styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="main">
    <table>

        <?php for($i = 0; $i < $game->getRows(); $i++){ ?>
            <tr>

            <?php for($y = ($i % 2); $y < (($i % 2) + $game->getCols()); $y++) { ?>
                    <td data-token="<?php echo "$i.$y"?>" class="<?php echo array_key_exists("$i.$y",$selectedTokens)?$selectedTokens["$i.$y"]['state']:"hidden"?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm4 14.083c0-2.145-2.232-2.742-3.943-3.546-1.039-.54-.908-1.829.581-1.916.826-.05 1.675.195 2.443.465l.362-1.647c-.907-.276-1.719-.402-2.443-.421v-1.018h-1v1.067c-1.945.267-2.984 1.487-2.984 2.85 0 2.438 2.847 2.81 3.778 3.243 1.27.568 1.035 1.75-.114 2.011-.997.226-2.269-.168-3.225-.54l-.455 1.644c.894.462 1.965.708 3 .727v.998h1v-1.053c1.657-.232 3.002-1.146 3-2.864z"/></svg>
                    </td>
            <?php } ?>

            </tr>
    <?php } ?>
    </table>
    <input type="button" value="Start" id="start" style="float: left;"><div style="float: left;">Elapsed time:<div id="timer"><?php echo $elapsedTime;?></div></div>
    <input type="hidden" id="timer_id" value="">
</div>
<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
<script src="actions.js"></script>
</body>
</html>


