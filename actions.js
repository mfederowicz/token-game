$('td').click(function (e) {
    var $this = $(this);
    var tokenid = $this.attr('data-token');

    $.ajax({
        url: '?action=checktoken&id=' + tokenid,
        type: 'get',
        dataType: 'json',
        async: true,
        success: function (data) {
            if (data == "win" || data == "show") {
                $this.removeClass('hidden');
                $this.addClass(data);
                if (data == "win") {
                    clearInterval($('#timer_id').val());
                    alert('Gratulation you win');
                }
            } else {

                if (data == "invalid game click star") {
                    $('td').each(function () {
                        $(this).removeClass('win');
                        $(this).removeClass('show');
                        $(this).addClass('hidden');
                    });
                }

                clearInterval($('#timer_id').val());
                alert(data);
            }
        }

    });
    e.preventDefault();


});
$('#start').click(function (e) {
    var $this = $(this);
    var tokenid = $this.attr('data-token');

    $.ajax({
        url: '?action=startgame',
        type: 'get',
        dataType: 'json',
        async: true,
        success: function (data) {

            $('td').each(function () {
                $(this).removeClass('win');
                $(this).removeClass('show');
                $(this).addClass('hidden');
            });
            $('#timer').html('60');
            var interval = setInterval(function () {
                v = $('#timer').html();
                var new_val = parseInt(v) - 1;

                if (new_val < 1) {
                    clearInterval($('#timer_id').val());
                    alert('Finish game');
                } else {
                    $('#timer').html(new_val);
                }


            }, 1000);
            $('#timer_id').val(interval);


        }

    });
    e.preventDefault();
});